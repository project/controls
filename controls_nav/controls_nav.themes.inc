<?php
/**
 * @file
 * Babcock menu - Controls the display of the menu at the top of the page - themes.
 */

function theme_controls_nav($elements) {
  $elements['#attributes']['class'] = 'controls-nav controls-nav-level-' . $elements['#level'];

  if ($elements['#children'] == '') {
    if (!isset($elements['#sorted'])) {
      uasort($elements, "element_sort");
    }
    $keys = element_children($elements);
    $max = count($keys) - 1;
    $count = 0;
    foreach ($keys as $key) {
      if (!isset($elements[$key]['#attributes']['class'])) {
        $elements[$key]['#attributes']['class'] = '';
      }
      if (!isset($elements[$key]['#options']['attributes']['class'])) {
        $elements[$key]['#options']['attributes']['class'] = '';
      }
      if ($count==0) {
        $elements[$key]['#attributes']['class'] .= ' first';
        $elements[$key]['#options']['attributes']['class'] .= ' first';
      }
      if ($count==$max) {
        $elements[$key]['#attributes']['class'] .= ' last';
        $elements[$key]['#options']['attributes']['class'] .= ' last';
      }
      $elements['#children'] .= drupal_render($elements[$key]);
      $count++;
    }
  }

  return '<ul' . drupal_attributes($elements['#attributes']) . '>' . $elements['#children'] . '</ul>';
}

function theme_controls_nav_item($element) {
  static $path, $alias;
  if (!isset($path)) {
    $path = $_GET['q'];
    $alias = drupal_get_path_alias($path);
  }

  $control = $element['#control'];

  $element['#children'] = is_array($element['#children']) ? drupal_render($element['#children']) : '';

  $active = FALSE;
  if (isset($control['active']) && is_array($control['active']) && !empty($control['active'])) {
    $activators = $control['active'];
    if (isset($activators['include']) && (drupal_match_path($alias, $activators['include']) || drupal_match_path($path, $activators['include']))) {
      $active = TRUE;
    }
    if (isset($activators['exclude']) && (drupal_match_path($alias, $activators['exclude']) || drupal_match_path($path, $activators['exclude']))) {
      $active = FALSE;
    }
    if (isset($activators['entity_type'])) {
      $entity = menu_get_object($activators['entity_type'], $activators['position']?$activators['position']:1);
      if ($entity) {
        list(,,$bundle) = entity_extract_ids($activators['entity_type'], $entity);
        $active = in_array($bundle, $activators['bundles']);
      }
    }
  }

  if ($active) {
    if (!isset($element['#attributes']['class'])) {
      $element['#attributes']['class'] = '';
    }
    if (!isset($element['#options']['attributes']['class'])) {
      $element['#options']['attributes']['class'] = '';
    }
    $element['#attributes']['class'] .= ' active';
    $element['#options']['attributes']['class'] .= ' active';
  }

  $link = l($element['#title'], $element['#href'], $element['#options']) . $control['extra'];
  return ($link!='') ? '<li' . drupal_attributes($element['#attributes']) . '>' . $link . $element['#children'] . '</li>' : '';
}
