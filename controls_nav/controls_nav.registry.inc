<?php
/**
 * @file
 * Controls navigation - Provides a separate way of building a main navigation menu - registry.
 */

/**
 * Implements hook_theme().
 */
function _controls_nav_theme() {
  return array(
    'controls_nav' => array(
      'render element' => 'elements',
      'file' => 'controls_nav.themes.inc',
    ),
    'controls_nav_item' => array(
      'render element' => 'element',
      'file' => 'controls_nav.themes.inc',
    ),
  );
}
